import {Row} from './js/components.js';
import {arrayToMap} from './js/utils.js';

/* Initial function to render application
 * @param {object} rawData
 */
export function render(data) {
	// Create fragment in memory so that we interact only once with the DOM
	const root = document.querySelector('.gallery');
	const fragment = document.createDocumentFragment();

	// Render each list in memory
	const rows = preprocess(data);
	for (let row of rows) {
		fragment.appendChild(Row(row));
	};

	// Add fragment to dom
	root.appendChild(fragment);
};

/* Preprocess data to be able to find videoIDs and billboard rows in O(1)
 * @param {object] rawData - as found in data.js
 * @return {object} {
 *	 billboards: Map<Number, Object>
 *   videos: Map<Number, Object>
 *   rows: Number[]
 * }
 */
function preprocess(rawData) {
	const videos = arrayToMap(rawData.videos, 'id');
	const billboards = arrayToMap(rawData.billboards, 'row');

	return rawData.rows.map((row, i) => {
		return {
			...billboards.get(i),
			videos: row.map(id => videos.get(id))
		};
	});
}
