import {RowClasses, VideoImageKey, ShouldObserveAnimation} from './billboard-constants.js';
import {element, cssurl} from './utils.js';

/* Renders a single Billboard component
 * @param {Object} props {
 *    {Object[]} videos
 *    {String} type
 *    {Object} ...other - used to render Billboard
 * }
 *
 * @return HTMLElement
 */
export function Row({videos, type, ...other}) {
	const items = videos.map(video => {
		return type ?
			Billboard({video, type, ...other}) :
			Video({video})
	});

	return element(
		'div',
		[
			'row-videos',
			...(RowClasses[type] || [])
		],
		items
	);
}

/* Renders a single Video component
 * @param {Object} props {
 *    {Object} video
 * }
 *
 * @return HTMLElement
 */
export function Video({video}) {
	return element(
		'div',
		'boxshot',
		null,
		null,
		{backgroundImage: cssurl(video.boxart)}
	);
}

/* Renders a single Billboard component
 * @param {Object} props {
 *    {Object} video
 *    {String} type
 *    {Object[]} buttons
 * }
 *
 * @return HTMLElement
 */
export function Billboard({video, type, buttons}) {
	const {logo, synopsis} = video || {};

	// Create billboard content
	const metadata = element('div', 'billboard-metadata', [
		Logo({logo}),
		Synopsis({synopsis}),
		...buttons.map(Button)
	]);

	// For the relevant types of billboard, add an observer to animate metadata content
	// Initial metadata state must be 'hidden'
	if (ShouldObserveAnimation[type]) {
		metadata.classList.add('hidden');
		const observer = new IntersectionObserver(([entry]) => {
			// as soon as it intersects, make it visible and stop observing
			if (entry.isIntersecting) {
				entry.target.classList.remove('hidden');
				observer.unobserve(entry.target);
			}
		});
		observer.observe(metadata);
	}

	return element(
		'div',
		'billboard-background',
		metadata,
		null,
		{backgroundImage: cssurl(video[VideoImageKey[type]])}
	);
}

/* Renders a Button component
 * @param {Object} props {
 *    {string} text
 *    {String} type
 * }
 *
 * @return HTMLElement
 */
export function Button({text, type}) {
	return element(
		'button',
		['billboard-metadata-button', `billboard-metadata-button-${type}`],
		text
	)
}

/* Renders a Button component
 * @param {Object} props {
 *    {string} synopsis
 * }
 *
 * @return HTMLElement
 */
export function Synopsis({synopsis}) {
	return synopsis ?
		element('div', 'billboard-metadata-synopsis', synopsis) :
		null;
}

/* Renders a Button component
 * @param {Object} props {
 *    {string} logo
 * }
 *
 * @return HTMLElement
 */
export function Logo({logo}) {
	return logo ?
		element('img', 'billboard-metadata-logo', null, {src: logo}) :
		null;
}
