export const RowClasses = {
	header: ['row-billboard'],
	inline: ['row-billboard', 'row-billboard-inline']
};

export const VideoImageKey = {
	inline: 'backgroundShort',
	header: 'background'
};

export const ShouldObserveAnimation = {
	inline: true
};