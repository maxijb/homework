
/* Creates an HTML element based on props
 * @param {string} type
 * @param {string|[string]} className
 * @param {string|string[]|HTMLElement|[HTMLElement[]} children
 * @param {Object} attrs
 * @param {Object} style
 * @return {HTMLElement}
 */
export function element(type, className, children, attrs, style) {
	const elm = document.createElement(type);

	// Adding class names
	if (className) {
		elm.classList.add(...makeArray(className));
	}

	// Adding children
	if (children) {
		makeArray(children).forEach(node => {
			// omit empty nodes
			if (!node) {
				return;
			}

			if (typeof node === 'string' || typeof node === 'number') {
				const txt = document.createTextNode(node);
				elm.appendChild(txt);
			} else {
				elm.appendChild(node)
			}
		});
	}

	// Adding attributes
	if (attrs) {
		Object.keys(attrs).forEach(attr => {
			elm[attr] = attrs[attr];
		});
	}

	// Adding style
	if (style) {
		Object.keys(style).forEach(attr => {
			elm.style[attr] = style[attr];
		});
	}

	return elm;
}


/* Converts an array into a map using `key` as the hashing key
 * @param {Object[]} arr
 * @param {string} key
 * @return Map<string|number, Object>
 */
export function arrayToMap(arr, key) {
	return new Map(
		arr.map(it => [it[key], it])
	);
}

/* Wraps a url into the format required by css rules
 * @param {string} uri
 * @return {string}
 */
export function cssurl(uri) {
	return `url(${uri})`;
}

/* Converts single items to an array.
 * If item is already an array, returns the same reference as found in input
 * @param {Array|any} item
 * @return {Array}
 */
function makeArray(item) {
	return Array.isArray(item) ? item : [item];
}